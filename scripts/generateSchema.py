

def generateKeyList(modifier):
    template = """
    <key type="as" name="resize-grid-loc-{}-{}">
        <default><![CDATA[['{}']]]></default>
                <summary>TestKey</summary>
    </key>"""

    keyIter = iter("1234567qwertyuasdfghjzxcvbnm")
    keySchema = ""
    for j in range(4):
        for i in range(7):
            keySchema += template.format(i,j,modifier+next(keyIter))

    return keySchema

schema=f"""
<schemalist>
  <schema id="org.gnome.shell.extensions.wesizer" path="/org/gnome/shell/extensions/wesizer/">
  {generateKeyList("<Ctrl><Super>")}
  </schema>
</schemalist>
"""

print(schema)
