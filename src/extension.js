
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Lang = imports.lang;
const Convenience = imports.misc.extensionUtils.getCurrentExtension().imports.convenience;
let tracker;
let cur_top_left = [3,0]
let cur_bottom_right = [7,4]
let last_keystroke = [0,0,0]
keyboardExtend = [7,4]

let text, button;

function debug_log(...args){
    // arguments.unshift("[wesizer log]:");
    // log.apply(null,arguments)
    log("[WESIZER_LOG]", ...args)
}

function _hideHello() {
    Main.uiGroup.remove_actor(text);
    text = null;
}

function show_tween(message) {
    if (!text) {
        text = new St.Label({ style_class: 'helloworld-label', text: message });
        Main.uiGroup.add_actor(text);
    }

    text.opacity = 255;

    let monitor = Main.layoutManager.primaryMonitor;

    text.set_position(monitor.x + Math.floor(monitor.width / 2 - text.width / 2),
                      monitor.y + Math.floor(monitor.height / 2 - text.height / 2));

    Tweener.addTween(text,
                     { opacity: 0,
                       time: 0.5,
                       transition: 'easeOutQuad',
                       onComplete: _hideHello });
}

function init() {
    tracker = Shell.WindowTracker.get_default();
}

function get_key_name(i,j) {
    return 'resize-grid-loc-'+i+'-'+j;
}

function enable() {
    for (j = 0; j < keyboardExtend[1]; j++){
        for (i = 0; i < keyboardExtend[0]; i++) { 
            Main.wm.addKeybinding(
                get_key_name(i,j),
                Convenience.getSettings(),
                Meta.KeyBindingFlags.NONE,
                Shell.ActionMode.NORMAL,
                function (i,j) {
                    return function (evt) {
                        resize_window(i,j,evt);
                    }
                } (i,j)
            );
        }
    }
}

function disable() {
    for (j = 0; j < keyboardExtend[1]; j++){
        for (i = 0; i < keyboardExtend[0]; i++) { 
            Main.wm.removeKeybinding(get_key_name(i,j));
        }
    }
}

function sub(a,b){
    return [a[0]-b[0],a[1]-b[1]]
}

function resize_window(i,j,evt){
    // print_all(evt)
    let cur_time = new Date().getTime();
    let cur_size = sub(cur_bottom_right, cur_top_left)
    let cur_block = sub([i,j],  cur_top_left)
    let focusedWindow = getFocusApp();
    let cur_last_keystroke = last_keystroke.slice();
    last_keystroke[2]=-1;
    let last_block = sub(cur_last_keystroke, cur_top_left)
    let delta_size = sub([i,j],cur_last_keystroke)
        
    if (delta_size[0] < 0 && delta_size[1]<0 && (cur_time - cur_last_keystroke[2]) < 2000){
        cur_top_left = [i,j]
        cur_bottom_right = [cur_last_keystroke[0]+1,cur_last_keystroke[1]+1]
        debug_log("changing grid loc",cur_top_left, cur_bottom_right);
        show_tween("New window formation:"+ cur_top_left + ", " + cur_bottom_right)
        return;
    }

    if (!focusedWindow || cur_block[0] < 0 || cur_block[1] < 0 || cur_block[0] >= cur_size[0] || cur_block[1] >= cur_size[1]) {
        debug_log("No focused window or index out of current grid - ignoring keyboard shortcut");
        debug_log(cur_block[0], cur_size[0], cur_block[1], cur_size[1])
        last_keystroke = [i,j,cur_time]
        return;
    }

    if (focusedWindow.maximized_horizontally || focusedWindow.maximizedVertically){
        focusedWindow.unmaximize(Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL);
    }

    let max_width = global.screen.get_active_workspace().get_work_area_for_monitor(0).width
    let max_height = global.screen.get_active_workspace().get_work_area_for_monitor(0).height

    let delta_x = max_width / cur_size[0];
    let delta_y = max_height / cur_size[1];

    if (cur_time - cur_last_keystroke[2] < 2000){
        if (delta_size[0] < 0 || delta_size[1]<0){
            debug_log("negative size change:", delta_size, cur_block, cur_last_keystroke);
            return;
        }
        debug_log("changing size:", delta_x*last_block[0], delta_y*last_block[1], delta_x*delta_size[0]+1, delta_y*delta_size[1]+1);
        focusedWindow.move_resize_frame(true, delta_x*last_block[0], delta_y*last_block[1], delta_x*(delta_size[0]+1), delta_y*(delta_size[1]+1));
    }
    else{
        last_keystroke = [i,j,cur_time]
    }
}

function print_all(entity){
    debug_log(entity)
    for(var propName in entity) {
        propValue = entity[propName];
        debug_log(propName,propValue);
    }
}

function getFocusApp() {
    if (tracker.focus_app == null) {
        return false;
    }

    let focusedAppName = tracker.focus_app.get_name();

    let windows = global.screen.get_active_workspace().list_windows();
    let focusedWindow = false;
    for (let i = 0; i < windows.length; ++i) {
        let metaWindow = windows[i];
        if (metaWindow.has_focus()) {
            focusedWindow = metaWindow;
            break;
        }
    }

    return focusedWindow;
}

function reset_window(metaWindow) {
    metaWindow.unmaximize(Meta.MaximizeFlags.HORIZONTAL);
    metaWindow.unmaximize(Meta.MaximizeFlags.VERTICAL);
    metaWindow.unmaximize(Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL);
}
